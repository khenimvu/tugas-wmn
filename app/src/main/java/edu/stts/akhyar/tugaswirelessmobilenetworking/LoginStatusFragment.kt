package edu.stts.akhyar.tugaswirelessmobilenetworking

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import edu.stts.akhyar.tugaswirelessmobilenetworking.model.User
import kotlinx.android.synthetic.main.fragment_login_status.*

class LoginStatusFragment : Fragment() {
    companion object {
        fun newInstance() = LoginStatusFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login_status, container, false)
    }

    fun receiveUser(user: User){
        if(user.username.trim() == "" || user.password.toString().trim() == ""){
            status_log.append("Username atau password tidak boleh kosong\n")
        }else if( user.password.toString().trim() != "stts"){
            status_log.append("Passwordnya harus 'stts'\n")
        }else{
            sendToMainActivity(user)
        }
    }

    private fun sendToMainActivity(user: User) {
        val intent = Intent(this.context, MainActivity::class.java).apply {
            putExtra("EXTRA_USER", user)
        }
        startActivity(intent)
    }
}
