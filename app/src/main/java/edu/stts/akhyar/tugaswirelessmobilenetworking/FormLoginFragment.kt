package edu.stts.akhyar.tugaswirelessmobilenetworking

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import edu.stts.akhyar.tugaswirelessmobilenetworking.model.User
import kotlinx.android.synthetic.main.fragment_form_login.*

class FormLoginFragment : Fragment() {

    private lateinit var listener: FormLoginActionListener

    interface FormLoginActionListener {
        fun onFormLoginReady(user: User)
    }

    companion object {
        fun newInstance(listener: FormLoginActionListener): FormLoginFragment{
            val fragment = FormLoginFragment()
            fragment.listener = listener
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_form_login, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        button_login.setOnClickListener {
            val username = username_real.text.toString()
            val password = password_real.text.toString()

            sendToResultFragment(username, password)
        }
    }

    private fun sendToResultFragment(username: String, password: String) {
        val user = User(
                username,
                password
        )
        listener.onFormLoginReady(user)
    }
}
