package edu.stts.akhyar.tugaswirelessmobilenetworking

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import edu.stts.akhyar.tugaswirelessmobilenetworking.model.User

class LoginActivity : AppCompatActivity(), FormLoginFragment.FormLoginActionListener {
    private lateinit var loginStatusFragment: LoginStatusFragment
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loadFragmentForm()
        loadFragmentResult()
    }

    private fun loadFragmentResult() {
        this.loginStatusFragment = LoginStatusFragment.newInstance()
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.login_result_fragment, loginStatusFragment)
            commit()
        }
    }

    private fun loadFragmentForm() {
        val formLoginFragment = FormLoginFragment.newInstance(this)
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.form_login_fragment, formLoginFragment)
            commit()
        }
    }

    override fun onFormLoginReady(user: User) {
        loginStatusFragment.receiveUser(user)
    }
}
