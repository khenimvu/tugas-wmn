package edu.stts.akhyar.tugaswirelessmobilenetworking

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import edu.stts.akhyar.tugaswirelessmobilenetworking.model.User
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val user = intent.getParcelableExtra<User>("EXTRA_USER")
        welcome_message.text = "Welcome, ${user.username}"
    }

}
